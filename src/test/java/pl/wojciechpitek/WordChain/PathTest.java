package pl.wojciechpitek.WordChain;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PathTest extends Path {

    @Before
    public void setUp() {
        setWordList(prepareListWithSolution());
    }

    @Test
    public void getWordDiffFailTest() {
        assertFalse(getWordDiff("cat", "dog"));
        assertFalse(getWordDiff("wallmart", "brennann"));
        assertFalse(getWordDiff("wot", "hog"));
    }

    @Test
    public void getWordDiffOkTest() {
        assertTrue(getWordDiff("cat", "cot"));
        assertTrue(getWordDiff("cold", "gold"));
        assertTrue(getWordDiff("brain", "train"));
    }

    @Test
    public void findChildsFoundTwoChildsTest() {
        List<String> result = findChilds("dog");
        assertEquals(2, result.size());
        assertTrue(result.contains("cog"));
        assertTrue(result.contains("hog"));
    }

    @Test
    public void findChildsNoChildsTest() {
        List<String> result = findChilds("wizzard");
        assertTrue(result.isEmpty());
    }

    @Test
    public void findPathNoPathFoundTest() {
        setEndWord("lizard");
        List<String> path = buildPath(prepareWordMap());
        assertTrue(path.isEmpty());
    }

    @Test
    public void findPathOkTest() {
        setStartWord("cat");
        setEndWord("dog");
        List<String> path = buildPath(prepareWordMap());
        assertTrue(!path.isEmpty());
        assertTrue(path.contains("cat"));
        assertTrue(path.contains("cot"));
        assertTrue(path.contains("cog"));
        assertTrue(path.contains("dog"));
    }

    @Test
    public void buildMapTest() {
        setStartWord("cat");
        assertEquals(prepareAllWordsMap().size(), buildMap().size());
    }

    @Test
    public void getPathNoPathFoundTest() {
        setStartWord("lazy");
        setEndWord("busy");
        assertTrue(getPath().isEmpty());
    }

    @Test
    public void getPathOkTest() {
        setStartWord("cat");
        setEndWord("dog");
        List<String> result = getPath();
        assertFalse(result.isEmpty());
        assertEquals("cat", result.get(0));
        assertEquals("cot", result.get(1));
        assertEquals("cog", result.get(2));
        assertEquals("dog", result.get(3));
    }

    private List<String> prepareListWithSolution() {
        return Arrays.asList("cat", "cog", "dog", "cot", "wot", "hot", "hog");
    }

    private Map<String, String> prepareWordMap() {
        Map<String, String> map = new HashMap<>();
        map.put("dog", "cog");
        map.put("cog", "cot");
        map.put("cot", "cat");
        return map;
    }

    private Map<String, String> prepareAllWordsMap() {
        Map<String, String> map = new HashMap<>();
        map.put("dog", "cog");
        map.put("cog", "cot");
        map.put("cot", "cat");
        map.put("cat", "cot");
        map.put("hot", "cot");
        map.put("wot", "cot");
        map.put("hog", "cog");
        return map;
    }
}
