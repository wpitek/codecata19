package pl.wojciechpitek.WordChain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.internal.util.reflection.Whitebox;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

public class MainTest {
    private Main main;
    private Path path;

    @Before
    public void setUp() {
        main = spy(new Main("cat", "dog"));
        path = mock(Path.class);
        Whitebox.setInternalState(main, "path", path);
    }

    @Test
    public void printChainNoPathFoundTest() {
        when(path.getPath()).thenReturn(Collections.emptyList());
        main.printChain();
        verify(main, never()).printLine(anyString());
        verify(main, atLeastOnce()).printError(anyString());
    }

    @Test
    public void printChainOkTest() {
        when(path.getPath()).thenReturn(Arrays.asList("cat", "cot", "cog", "dog"));
        main.printChain();
        verify(main, times(4)).printLine(anyString());
        verify(main, never()).printError(anyString());
    }
}
