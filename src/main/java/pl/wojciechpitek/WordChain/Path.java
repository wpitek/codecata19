package pl.wojciechpitek.WordChain;

import lombok.Setter;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class Path {
    @Setter
    private List<String> wordList;
    @Setter
    private String startWord;
    @Setter
    private String endWord;

    List<String> getPath() {
        return buildPath(buildMap());
    }

    protected Boolean getWordDiff(String word1, String word2) {
        AtomicInteger integer = new AtomicInteger(0);
        for (int index = 0; index < word1.length(); index++) {
            if (word1.charAt(index) != word2.charAt(index)) {
                if (integer.incrementAndGet() > 1) {
                    break;
                }
            }
        }
        return integer.get() == 1;
    }

    protected List<String> findChilds(String word) {
        return wordList
                .stream()
                .filter(elem -> getWordDiff(elem, word))
                .collect(Collectors.toList());
    }

    protected Map<String, String> buildMap() {
        Map<String, String> wordMap = new HashMap<>();
        Queue<String> queue = new LinkedList<>(Collections.singletonList(startWord));
        while (!queue.isEmpty()) {
            final String word = queue.remove();
            findChilds(word).forEach(child -> {
                if (!wordMap.containsKey(child)) {
                    wordMap.put(child, word);
                    queue.add(child);
                }
            });
        }
        return wordMap;
    }

    protected List<String> buildPath(Map<String, String> wordMap) {
        List<String> path = new LinkedList<>();
        if (wordMap.containsKey(endWord)) {
            String word = endWord;
            while (!startWord.equals(word)) {
                path.add(word);
                word = wordMap.get(word);
            }
            path.add(startWord);
        }
        Collections.reverse(path);
        return path;
    }
}