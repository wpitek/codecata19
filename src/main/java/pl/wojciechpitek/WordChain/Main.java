package pl.wojciechpitek.WordChain;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

    private Path path;

    Main(String startWord, String endWord) {
        path = new Path();
        path.setWordList(initializeDictionary(startWord.length()));
        path.setStartWord(startWord);
        path.setEndWord(endWord);
    }

    public static void main(String[] args) {
        if (args.length < 2 || args[0].length() != args[1].length()) {
            throw new RuntimeException("Bad arguments! Program need at least two args with equal length!");
        }
        new Main(args[0], args[1])
                .printChain();
    }

    void printChain() {
        List<String> chain = path.getPath();
        if (!chain.isEmpty()) {
            chain.forEach(this::printLine);
        } else {
            printError("No solution available!");
        }
    }

    void printLine(String line) {
        System.out.println(line);
    }

    void printError(String error) {
        System.err.println(error);
    }

    private List<String> initializeDictionary(Integer length) {
        List<String> dictionary = new ArrayList<>();
        ClassLoader classLoader = getClass().getClassLoader();
        URL url = classLoader.getResource("wordlist.txt");
        if (url != null) {
            File file = new File(url.getFile());
            Scanner scanner = null;
            try {
                scanner = new Scanner(file);
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    if (length.equals(line.length())) {
                        dictionary.add(line);
                    }
                }
            } catch (FileNotFoundException e) {
                printError("Dictionary file not found in resources!");
            } finally {
                if (scanner != null) {
                    scanner.close();
                }
            }
        }
        return dictionary;
    }

}
